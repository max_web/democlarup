/****************************************************************
IMG-SPRITE

create sprite from PNG files
*****************************************************************/


const gulp         = require('gulp');
const config       = require('../config.js');
const imagemin     = require("gulp-imagemin");
const del          = require('del');
const spritesmith  = require('gulp.spritesmith');
const buffer       = require('vinyl-buffer');



gulp.task('sprite-png', () => {

    const spriteData = gulp.src(config.src.sprite)
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: '_sprite.scss',
            cssFormat: 'css',
            // cssVarMap(sprite) {
            //     sprite.name = 'icon-' + sprite.name;
            // },
            imgPath: '../img/sprite.png' 
        }));

    const imgStream = spriteData.img
    	.pipe(buffer())
        .pipe(imagemin([
            imagemin.optipng({ optimizationLevel: 3 }),
            imagemin.jpegtran({ progressive: true })
        ]))
        .pipe(gulp.dest( config.dest.img ));

    spriteData.css
        .pipe(gulp.dest(config.src.spriteStyles));

    return spriteData;
});


gulp.task('sprite-clean', (cb) => {
    del(`${config.dest.img}sprite.png`, cb);
});


gulp.task('sprite-png:watch', () => {
	gulp.watch( config.src.sprite,     ['sprite-png']);
});
