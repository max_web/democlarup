




/****************************************************************
JS

	compile with Babel
	concatenate
    minify
	add sourcemap for debugging
	watch changes

*****************************************************************/

const gulp         = require('gulp');
const watch        = require('gulp-watch');
const concat       = require('gulp-concat');
const sourcemaps   = require('gulp-sourcemaps');
const uglify       = require('gulp-uglify');
const babel        = require('gulp-babel');
const config       = require('../config.js');



gulp.task('js', function() {
    gulp.src( config.src.js )
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('scripts.js'), { newLine: ';' })
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest( config.dest.js ));
});




gulp.task('js:watch', function() {
    gulp.watch( config.src.js, ['js']);
});
