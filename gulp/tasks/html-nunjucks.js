/****************************************************************
HTML
	templating using nunjucks



Important
	page templates should be placed in template folder root , parts - in subfolder

*****************************************************************/

const gulp           = require('gulp');
const del            = require('del');
const config         = require('../config.js');
const nunjucksRender = require('gulp-nunjucks-render');
const plumber        = require('gulp-plumber');
const notify         = require("gulp-notify");
const frontMatter    = require('gulp-front-matter');
// const prettifyHTML   = require('gulp-prettify');



gulp.task('html-njk', () => {
    gulp.src(`${config.src.htmlTemplatesFolder}*+(.html|.nunjucks|.njk)`)

        .pipe(plumber({
            errorHandler(err) {
                // console.log( err );
                // console.log('Error in ' + err.plugin + '\n' + err.name + err.message);
				global.console.log(`
					Error in ${err.plugin}
					${err.name}
					${err.message}
				`);
				this.emit('end');
            }
        }))
        .pipe(frontMatter({ property: 'data' }))
        .pipe(nunjucksRender({
            path: [config.src.htmlTemplatesFolder]
        }))
        .on('error', notify.onError({ title: 'HTML compile error', message: 'Check the linter or console' }))
        // optional - format html for customers codestyle {% %}
        // .pipe(prettifyHTML({
        //     indent_size: 4,
        //     wrap_attributes: 'auto', // 'force'
        //     preserve_newlines: true,
        //     // unformatted: [],
        //     end_with_newline: true
        // }))
        .pipe(gulp.dest(config.dest.htmlTemplatesOutput));
});


gulp.task('html-njk:clean', () => {
    del.sync([`${config.dest.htmlTemplatesOutput}*.html`, '!app/index.html']);
});


gulp.task('html-njk:watch', () => {
    gulp.watch(`${config.src.htmlTemplatesFolder}**/*.*`, ['html-njk']);
});



