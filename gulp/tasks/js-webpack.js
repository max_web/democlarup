/****************************************************************
JS-WEBPACK

	Tasks
		bundle modules
		transpile code with Babel
		add sourcemap
		live-reload on changes
		show notification on error

	All settings are in webpack.config.js

*****************************************************************/


const gulp              = require('gulp');  
const webpackStream     = require('webpack-stream');
const webpack           = require('webpack');
const extend            = require('extend');
const config            = require('../config.js');
const webpackConfigFile = require('../../webpack.config.js');


gulp.task('js-webpack', () => {
	const webpackConfig = extend({}, webpackConfigFile, {
		watch: false,
	});

	gulp.src( config.src.jsEntryPoint ) 
		.pipe(  webpackStream( webpackConfig, webpack ))
		.pipe(gulp.dest( config.dest.js ));
});


gulp.task('js-webpack:watch', () => {
	const webpackConfig = extend({}, webpackConfigFile, {
		watch: true,
	});

	gulp.src( config.src.jsEntryPoint ) 
		.pipe(  webpackStream( webpackConfig, webpack ))
		.pipe(gulp.dest( config.dest.js ));
});
