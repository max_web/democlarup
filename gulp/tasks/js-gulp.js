




/****************************************************************
JS-GULP
	Tasks
		compile with Babel
		concatenate
	    minify
		add sourcemap for debugging
		watch changes
	Flag
		--dev disable babel and uglify

	Require
		Folder structure 
			app-src/
				js/
					_libs/
					plugins/
		In bash
			mkdir app-src/js/_libs app-src/js/plugins


*****************************************************************/

const gulp         = require('gulp');
const concat       = require('gulp-concat');
const sourcemaps   = require('gulp-sourcemaps');
const uglify       = require('gulp-uglify');
const babel        = require('gulp-babel');
const config       = require('../config.js');
const plumber      = require('gulp-plumber');
const notify       = require("gulp-notify");
const args         = require('yargs').argv;
const gulpif       = require('gulp-if');
        


gulp.task('js-gulp', () => {
    gulp.src( config.src.js )
        .pipe(plumber({
            errorHandler(err) {
                // console.log( err );
				global.console.log(`
					Error in ${err.plugin}
					${err.name}
					${err.message}
				`);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(gulpif(!args.dev, babel({
            presets: ['es2015']
        })) )
		.on('error', notify.onError({ title: 'JS build error', message: 'Check the console' }))
        .pipe(concat('bundle.js'), { newLine: ';' })
        .pipe(gulpif(!args.dev, uglify() ))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest( config.dest.js ));
});




gulp.task('js-gulp:watch', () => {
    gulp.watch( config.src.js, ['js-gulp']);
});
