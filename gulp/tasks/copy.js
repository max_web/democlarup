/****************************************************************
COPY

copy files to build folder
    .html
    favicons assets
    fonts
*****************************************************************/

const gulp   = require('gulp');
const config = require('../config.js');

gulp.task('copy:favicon', () => {
    gulp.src(config.src.favicon).pipe(gulp.dest(config.dest.favicon));
});

gulp.task('copy:fonts', () => {
    gulp.src(config.src.fonts).pipe(gulp.dest(config.dest.fonts));
});

gulp.task('copy:watch', () => {
    gulp.watch(config.src.favicon, ['copy:favicon']);
    gulp.watch(config.src.fonts, ['copy:fonts']);
});
