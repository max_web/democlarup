/****************************************************************
CSS
	compile SASS
    concatenate file
    postcss
	   autoprefixer
       unite @media
       minivacation - with correct sourcemap
	sourcemap - для отладки
	watch changes

console flas
    --dev - disable minification

*****************************************************************/

const gulp         = require('gulp');
const concat       = require('gulp-concat');
const sourcemaps   = require('gulp-sourcemaps');
const notify       = require("gulp-notify");
const config       = require('../config.js');

const sass         = require('gulp-sass');
const postcss      = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const mqpacker     = require("css-mqpacker");
// const minifyCSS    = require('postcss-csso');
const minifyCSS    = require('gulp-csso');

const args         = require('yargs').argv;
const gulpif       = require('gulp-if');

// mqpacker sort rule (DESC)
function isMax(mq) {
    return /max-width/.test(mq);
}
function isMin(mq) {
    return /min-width/.test(mq);
}
function sortMediaQueries(a, b) {
    const A = a.replace(/\D/g, '');
    const B = b.replace(/\D/g, '');

    if (isMax(a) && isMax(b)) {
        return B - A;
    } else if (isMin(a) && isMin(b)) {
        return A - B;
    } else if (isMax(a) && isMin(b)) {
        return 1;
    } else if (isMin(a) && isMax(b)) {
        return -1;
    }

    return 1;
}

gulp.task('css', () => {

    gulp.src( config.src.css )
        .pipe(sourcemaps.init() )

        .pipe(sass({outputStyle: 'expanded'} )
        .on('error', sass.logError))
        .on('error', notify.onError({title : 'SASS compile error in Gulp', message : 'Check the linter or console' }) )
        .pipe(concat('style.css'))

        .pipe(postcss([
            autoprefixer({browsers: ['last 4 versions', '> 1%', 'ie 8']}),
            mqpacker({ sort: sortMediaQueries }),
        ]))
        .pipe( gulpif( !args.dev, minifyCSS({ restructure: false }) ) ) // for correct sourcemap

        .pipe(sourcemaps.write() )
        .pipe(gulp.dest( config.dest.css ) );
});


gulp.task('css:watch', () => {
    gulp.watch( config.src.css, ['css']);
});




