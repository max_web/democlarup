/****************************************************************
Dev server with live reloading

	run browserSync with console flags (e.g. gulp --open)

	--open                # open in defoult browser
	--openf               # open в firefox
	--tunnel=siteName     # make tunnel for online demonstration

*****************************************************************/

const gulp         = require('gulp');
const browserSync  = require('browser-sync').create();
const config       = require('../config.js');
const args         = require('yargs').argv;  // add console flags feature (e.g. gulp --open)


/****************************************************************
Browsersync parameters from console flags
*****************************************************************/

let openBrowser = false;
let browserType = 'default';
let tunnelURL = false;
let isOnlineEnable = false;

if(args.open){
	openBrowser = true;
}
if(args.openf){
	openBrowser = true;
	browserType = ['firefox'];
}
if(args.tunnel){
	isOnlineEnable = true;
	tunnelURL = args.tunnel;
}


/****************************************************************
BrowserSync server settings
*****************************************************************/


gulp.task('server', () => {
  browserSync.init({
	server: {
	  baseDir: config.dest.root,
	},
	open: openBrowser,
	browser: browserType,

	online: isOnlineEnable,
	tunnel: tunnelURL,

	files:[  // autoreloading on changes instead of gulp watch
		config.dest.html,
		config.dest.css,
		config.dest.img,
		config.dest.js,
		config.dest.jsBundle,
		config.dest.fonts,
	],

	ghostMode:false,
	// ghostMode: {
	//     clicks: true,
	//     forms: true,
	//     scroll: false  // Disabled action synchronization to avoid conflicts with others test tools for responsive design
	// },

	scrollRestoreTechnique: 'cookie',
	logLevel: 'info', // 'debug', 'info', 'silent', 'warn'
    logConnections: false,
	logPrefix: "Browser-Sync",
	port: 8080,
	notify: false
  });
});

module.exports = browserSync;

