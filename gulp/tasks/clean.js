/****************************************************************
Clean
(clean build folder)
*****************************************************************/

const gulp         = require('gulp');  
const del          = require('del');
const config       = require('../config.js');


// gulp clean - очистка build
gulp.task('clean', () => {
	del.sync( config.dest.root ); 
});
