/****************************************************************
IMG

compress bitmap images
*****************************************************************/


const gulp         = require('gulp');
const config       = require('../config.js');
const imagemin     = require("gulp-imagemin");


gulp.task('img-optimize', () => {
    gulp.src(config.src.img)
        .pipe(imagemin([
            imagemin.optipng({ optimizationLevel: 3 }),
            imagemin.jpegtran({ progressive: true })
        ]))
        .pipe(gulp.dest(config.dest.img));
});



gulp.task('img-optimize:watch', () => {
	gulp.watch( config.src.img,     ['img-optimize']);
});
