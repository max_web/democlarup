const config = {
    src: {
    	root          : 'app-src/',
    	css           : 'app-src/sass/**/*.{sass,scss}',
		
    	// js build with webpack (task 'js-webpack')
		jsEntryPoint  : './app-src/js/app.js',

		// js build without webpack (task 'js-gulp')
    	js            :  [ 
				      	'!app-src/js/vendors/**/*.js', 
	                  	'app-src/js/_libs/**/*.js',
	                  	'app-src/js/plugins/**/*.js',
	                  	'app-src/js/**/*.js',
				        ],

		html          : 'app-src/*.html',
		favicon       : 'app-src/img/favicon-assets/*.*',
		fonts         : 'app-src/fonts/**/*.*',
		img           : [
							'!app-src/img/sprites/*.png',  
							'!app-src/img//favicon-assets/*.*',  
							'app-src/img/**/*.{png,jpg,gif}'
						],
		sprite        : 'app-src/img/sprites/*.png',
		spriteStyles  : 'app-src/sass/generated/', 
		svg           : 'app-src/img/sprites/*.svg',


		// html page tamplates should be located in folder root, parts in subfolder
		htmlTemplatesFolder: 'app-src/templates-html/', 

    },
    dest: {
    	root      : 'build/',
    	css       : 'build/css',
    	js        : 'build/js',
    	jsBundle  : 'build/js/bundle.js',
		html      : 'build/*.html',
		fonts     : 'build/fonts/',
		img       : 'build/img',
		favicon   : 'build/img/favicon',

		htmlTemplatesOutput: 'build/' 
    },

};




module.exports = config;
