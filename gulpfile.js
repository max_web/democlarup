



const gulp         = require('gulp');
const runSequence  = require("run-sequence");
const requireDir   = require('require-dir');
const dir          = requireDir('./gulp', {recurse: true});






/****************************************************************
Main tasks
*****************************************************************/


gulp.task('build', function(cb) {
    runSequence(
        'clean',

        [
            'html-njk', //templating with nunjucks
            'css',
            'js-webpack'
            // 'js-gulp',
        ],

        'copy:favicon',
        'copy:fonts',
        'sprite-png',
        'img-optimize',
        cb
    );
});


gulp.task('default', function(cb) {
    runSequence(
        'build',
        [                        //run task in parallel
            'html-njk:watch',
            'css:watch',
            'js-webpack:watch',
            // 'js-gulp:watch',
            'sprite-png:watch',
            'img-optimize:watch'
        ],
        'server',
        cb
    );
});
