const path                        = require("path");
const webpack                     = require("webpack");
const config                      = require('./gulp/config.js');
const WebpackBuildNotifierPlugin  = require('webpack-build-notifier');

module.exports = {
	entry: config.src.jsEntryPoint,
	output: {
		path: path.resolve(__dirname, config.dest.js),
		filename: "bundle.js"
	},
	devtool: 'source-map', 
	// watch: true,
	module: {
		rules: [
			// transpile code with Babel 
			{
				test: /\.js$/, 
				exclude: [/node_modules/], 
				use: [{ 
					loader: 'babel-loader',
					options: { presets: ['es2015'] }
					}]
			},
		]
	},
	plugins: [
		// add jQuery globally for jQuery plugins 
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		}),
	
		// Compress js
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true
		}),
		
		// Add tray notification on error
		new WebpackBuildNotifierPlugin({
			title: "JavaScript webpack",
			suppressSuccess: 'always',
			messageFormatter(){ // compact notify message
				return "Check the linter or console";
			}
		}),

	]
};
