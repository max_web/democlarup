import $ from 'jquery';
import 'slick-carousel';



/****************************************************************
Menu - mobile version
*****************************************************************/
const $menuBtn = $('.btn-menu-mobile');
const $menu = $('.menu');

// show menu
$menuBtn.on('click', () => {
    $menu.slideToggle('fast');
});

// view on windows resize
$(window).on('resize orientationchange', () => {
    if ($(window).outerWidth(true) > 900) {
        $menu.show();
    }
});



/****************************************************************
Testimonials with Slick Slider
*****************************************************************/

$(document).ready(() => {
	$('.slider-wrapper').slick({
		dots: true,
		infinite: true,
		slide: '.testimonials__item',
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	});
});
