#How to use

Clone this repo and then in command line type:

* `npm install` or `yarn` - install all dependencies
* `gulp` - run dev-server and build project
* `gulp build` - build project from sources



## List of Gulp tasks

To run separate task type in command line `gulp [task_name]`.
Almost all tasks also have watch mode - `gulp [task_name]:watch`, but you don't need to use it directly.

### Main tasks
Task name          | Description
:------------------|:----------------------------------
`default`          | will start all tasks required by project in dev mode: initial build, watch files, run server with livereload
`build`            | build production-ready project

### Other tasks
Task name          | Description
:------------------|:----------------------------------
`css`              | compile .sass/.scss to .css. We also use [postcss](https://github.com/postcss/postcss) for autoprefixer and mqpacker(to unite media query), so feel free to include other  [postcss plugins](https://github.com/postcss/postcss#plugins) when needed
`js-gulp`          | compile .js sources into bundle file with gulp modules (optionally)
`js-webpack`       | compile .js sources into bundle file with Webpack
`html-njk`         | compile Mozilla's awesome [nunjucks](https://mozilla.github.io/nunjucks/) html templates (data can be stored in YAML)
`sprite-png`       | create png sprite
`img-optimize`     | compress bitmap images
`copy`             | copy common files from `./src` path to `./dist` path
`clean`            | remove `./build` folder
`server`           | run dev-server powered by [BrowserSync](https://www.browsersync.io/)

_All available tasks are placed in a folder `./gulp/tasks` as separate *.js files. Usually, file name = task name._


## Flags

We have several useful flags.

* `gulp --open` - run dev server and then open preview in browser
* `gulp --dev` - build project without css minification
* `gulp server --tunnel [name]` - runs dev server and allows you to easily share a web service on your local development machine (powered by [localtunnel.me](https://localtunnel.me/)). Your local site will be available at `[name].localtunnel.me`.


##Other

You can also use [npm scripts](https://docs.npmjs.com/misc/scripts):

* `npm run start` - same as `gulp default`.
* `npm run build` - same as `gulp build`.
